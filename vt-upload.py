#!/usr/bin/python

import requests
import json
import optparse
  
parser = optparse.OptionParser()
parser.add_option('-a', '--apikey',
    action="store", dest="apikey",
    help="VirusTotal API key", default="")
parser.add_option('-f', '--file',
    action="store", dest="file",
    help="File to analyze", default="")

options, args = parser.parse_args()

if options.apikey == "":
    print("PROVIDE AN API KEY!")
    exit(1)

if options.file == "":
    print("PROVIDE A FILE FOR ANALYSIS!")
    exit(1)

params = {'apikey': options.apikey}
files = {'file': (options.file, open(options.file, 'rb'))}
response = requests.post('https://www.virustotal.com/vtapi/v2/file/scan', files=files, params=params)
json_response = json.dumps(response.json())
json_fixed = json.loads(json_response)
resource = json_fixed['resource']

params = {'apikey': options.apikey, 'resource': resource}
headers = {
  "Accept-Encoding": "gzip, deflate",
  "User-Agent" : "gzip,  My Python requests library example client or username"
  }
response = requests.get('https://www.virustotal.com/vtapi/v2/file/report',
  params=params, headers=headers)
print json.dumps(response.json())