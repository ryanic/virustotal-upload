# Description
Using your free or paid VirusTotal API key, uploads a file for analysis. Repeat later to get results.

# Usage

```
Usage: vt-upload.py [options]

Options:
  -h, --help            show this help message and exit
  -a APIKEY, --apikey=APIKEY
                        VirusTotal API key
  -f FILE, --file=FILE  File to analyze
```

# For 'Prettier' Output...
Install jq (Linux) and run the following:
```
python vt-upload.py -a APIKEY -f FILE | jq .
```

# Demonstration

![](https://gitlab.com/ryanic/virustotal-upload/raw/master/demo_vt-upload.gif)